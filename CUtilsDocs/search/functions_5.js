var searchData=
[
  ['vector_5fdestroy_34',['vector_destroy',['../vector_8c.html#aa24d3de7cd65801f1edb55c1741217f1',1,'vector_destroy(struct vector *vec):&#160;vector.c'],['../vector_8h.html#aa24d3de7cd65801f1edb55c1741217f1',1,'vector_destroy(struct vector *vec):&#160;vector.c']]],
  ['vector_5finit_35',['vector_init',['../vector_8c.html#a22a0cfc7b4e74f6f49868ce7264fb12b',1,'vector_init(struct vector *vec, size_t memb_size, uint32_t nmemb):&#160;vector.c'],['../vector_8h.html#a22a0cfc7b4e74f6f49868ce7264fb12b',1,'vector_init(struct vector *vec, size_t memb_size, uint32_t nmemb):&#160;vector.c']]],
  ['vector_5finsert_36',['vector_insert',['../vector_8c.html#a85a28405d126106995053c48340cebaa',1,'vector_insert(struct vector *vec, void *memb):&#160;vector.c'],['../vector_8h.html#a85a28405d126106995053c48340cebaa',1,'vector_insert(struct vector *vec, void *memb):&#160;vector.c']]]
];
